<?php
/**
 * Plugin Name: Contact Page Posts
 * Plugin URI: http://www.mattjennings.net/
 * Description: Plugin displays Contact page custom post types.
 * Author: Matt Jennings
 * Author URI: http://www.mattjennings.net/
 * Version: 0.0.1
 * License: GPLv2
 */
// Exit if accessed directly
if(!defined('ABSPATH')) {
  exit;
}


/******
 * Contact Page Post Type START
 ******/

function contact_page_post_type() {
  $singular = 'Contact Page';
  $plural = $singular;
  $labels = array(
        'name'                  => $plural,
        'singular_name'         => $singular,
        'add_name'              => 'Add New',
        'add_new_item'          => 'Add New ' . $singular,
        'edit'                  => 'Edit',
        'edit_item'             => 'Edit ' . $singular,
        'new_item'              => 'New ' . $singular,
        'view'                  => 'View ' . $singular,
        'view_item'             => 'View' . $singular,
        'search_term'           => 'Search ' . $plural,
        'parent'                => 'Parent ' . $singular,
        'not_found'             => 'No ' . $plural . ' found',
        'not_found_in_trash'    => 'No ' . $plural . ' in Trash'
  );
  $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'show_in_nav_menus'     => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_admin_bar'     => true,
        'menu_position'         => 50,
        'menu_icon'             => 'dashicons-megaphone',
        'can_export'            => true,
        'delete_with_user'      => false,
        'hierarchical'          => false,
        'has_archive'           => false,
        'query_var'             => true,
        'capability_type'       => 'post',
        'map_meta_cap'          => true,
        'rewrite'               => array(
              'slug'                  => 'home-page-posts',
              'with_front'            => true,
              'pages'                 => true,
              'feeds'                 => true
        ),
        'supports'              => array(
              'title',
              'editor',
              'thumbnail'
        )
  );
  register_post_type('contactpage', $args);
}
add_action('init', 'contact_page_post_type');

function contact_page_add_meta_box() {
  add_meta_box(
        'contact_page_id',
        __( 'Contact Info', 'contact_page_plugin' ),
        'contact_page_meta_box_callback',
        'contactpage'
  );
}

function contact_page_meta_box_callback($post) {
  wp_nonce_field( 'contact_page_save_meta_box_data', 'contact_page_meta_box_nonce' );

  $contact_page_email_value = get_post_meta($post->ID, '_contact_page_email', true);
  $contact_page_phone_number_value = get_post_meta($post->ID, '_contact_page_phone_number', true);

  echo '<p><label for="contact_page_email">Email</label><br /><input type="text" name="contact_page_email" id="' . $post->ID . '" value="' . $contact_page_email_value . '" style="width: 100%;" /></p>';
  echo '<p><label for="contact_page_phone_number">Phone Number<br /><span style="color: green; font-weight: bold;">*Numbers and dashes ONLY with NO spaces like:<br />555-444-1212</span></label><br /><input type="text" name="contact_page_phone_number" id="' . $post->ID . '" value="' . substr($contact_page_phone_number_value, 0, 3) . '-' . substr($contact_page_phone_number_value, 3, 3) . '-' . substr($contact_page_phone_number_value, 6, 4) . '" style="width: 100%;" /></p>';
}

add_action( 'add_meta_boxes', 'contact_page_add_meta_box' );

function contact_page_save_meta_box_data( $post_id ) {

  if ( !isset($_POST['contact_page_meta_box_nonce']) ) {
    return;
  }

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }

  if ( ! current_user_can( 'edit_post', $post_id ) ) {
    return;
  }

  if ( !isset($_POST['contact_page_email']) && !isset($_POST['contact_page_phone_number']) ) {
    return;
  }

  $data_contact_page_email = sanitize_text_field( $_POST['contact_page_email'] );

  // Variable below:
  // - Removes all entered characters in INPUT tag except for numbers.
  // - Of remaining numbers removes ANY that are over 10 to ensure that a
  //   correct phone number, with an area code, is inserted.
  $data_contact_page_phone_number = substr(preg_replace("/[^0-9]/", "", $_POST['contact_page_phone_number']), 0, 10);

  update_post_meta( $post_id, '_contact_page_email', $data_contact_page_email );
  update_post_meta( $post_id, '_contact_page_phone_number', $data_contact_page_phone_number );
}

add_action( 'save_post', 'contact_page_save_meta_box_data' );

/******
 * Contact Page Post Type END
 ******/