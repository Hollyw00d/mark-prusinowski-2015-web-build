<?php
/*
 *  Author: Matt Jennings | http://www.mattjennings.net/
 *  URL: http://www.mattjennings.net/
 *  Custom functions, support, custom post types and more.
 */


    // mpministry2015 Blank navigation
    function mpministry2015_nav()
    {
        wp_nav_menu(
        array(
            'theme_location'  => 'header-menu',
            'menu'            => '',
            'container'       => 'div',
            'container_class' => 'menu-{menu slug}-container',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => '',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 0,
            'walker'          => ''
            )
        );
    }

    // Load Blank scripts (header.php)
    function mpministry2015_header_scripts()
    {
        if ($GLOBALS['pagenow'] != 'wp-login.php' && !is_admin()) {

            wp_register_script('conditionizr', get_template_directory_uri() . '/js/lib/conditionizr-4.3.0.min.js', array(), '4.3.0'); // Conditionizr
            wp_enqueue_script('conditionizr'); // Enqueue it!

            wp_register_script('modernizr', get_template_directory_uri() . '/js/lib/modernizr-2.7.1.min.js', array(), '2.7.1'); // Modernizr
            wp_enqueue_script('modernizr'); // Enqueue it!

            wp_register_script('mpministry2015', get_template_directory_uri() . '/js/scripts.js', array('jquery'), '1.0.0'); // Custom scripts
            wp_enqueue_script('mpministry2015'); // Enqueue it!
        }
    }

    // Load conditional scripts
    function mpministry2015_conditional_scripts()
    {
        if (is_page('pagenamehere')) {
            wp_register_script('scriptname', get_template_directory_uri() . '/js/scriptname.js', array('jquery'), '1.0.0'); // Conditional script(s)
            wp_enqueue_script('scriptname'); // Enqueue it!
        }
    }

    // Load styles
    function mpministry2015_styles()
    {
        wp_register_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '3.3.5', 'all');
        wp_enqueue_style('bootstrap'); // Enqueue it!


        wp_register_style('mainstyle', get_template_directory_uri() . '/style.css', array(), '1.0', 'all');

        wp_enqueue_style('mainstyle'); // Enqueue it!
    }

    // Register mpministry2015 Blank Navigation
    function register_mpministry2015_menu()
    {
        register_nav_menus(array( // Using array to specify more menus if needed
            'header-menu' => __('Header Menu', 'mpministry2015'), // Main Navigation
            'sidebar-menu' => __('Sidebar Menu', 'mpministry2015'), // Sidebar Navigation
            'extra-menu' => __('Extra Menu', 'mpministry2015') // Extra Navigation if needed (duplicate as many as you need!)
        ));
    }

    // Remove the <div> surrounding the dynamic navigation to cleanup markup
    function my_wp_nav_menu_args($args = '')
    {
        $args['container'] = false;
        return $args;
    }

    // Remove Injected classes, ID's and Page ID's from Navigation <li> items
    function my_css_attributes_filter($var)
    {
        return is_array($var) ? array() : '';
    }

    // Remove invalid rel attribute values in the categorylist
    function remove_category_rel_from_category_list($thelist)
    {
        return str_replace('rel="category tag"', 'rel="tag"', $thelist);
    }

    // Add page slug to body class, love this - Credit: Starkers Wordpress Theme
    function add_slug_to_body_class($classes)
    {
        global $post;
        if (is_home()) {
            $key = array_search('blog', $classes);
            if ($key > -1) {
                unset($classes[$key]);
            }
        } elseif (is_page()) {
            $classes[] = sanitize_html_class($post->post_name);
        } elseif (is_singular()) {
            $classes[] = sanitize_html_class($post->post_name);
        }

        return $classes;
    }

    // If Dynamic Sidebar Exists
    if (function_exists('register_sidebar'))
    {
        // Define Sidebar Widget Area 1
        register_sidebar(array(
            'name' => __('Widget Area 1', 'mpministry2015'),
            'description' => __('Description for this widget-area...', 'mpministry2015'),
            'id' => 'widget-area-1',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));

        // Define Sidebar Widget Area 2
        register_sidebar(array(
            'name' => __('Widget Area 2', 'mpministry2015'),
            'description' => __('Description for this widget-area...', 'mpministry2015'),
            'id' => 'widget-area-2',
            'before_widget' => '<div id="%1$s" class="%2$s">',
            'after_widget' => '</div>',
            'before_title' => '<h3>',
            'after_title' => '</h3>'
        ));
    }

    // Remove wp_head() injected Recent Comment styles
    function my_remove_recent_comments_style()
    {
        global $wp_widget_factory;
        remove_action('wp_head', array(
            $wp_widget_factory->widgets['WP_Widget_Recent_Comments'],
            'recent_comments_style'
        ));
    }

    // Pagination for paged posts, Page 1, Page 2, Page 3, with Next and Previous Links, No plugin
    function mpministry2015wp_pagination()
    {
        global $wp_query;
        $big = 999999999;
        echo paginate_links(array(
            'base' => str_replace($big, '%#%', get_pagenum_link($big)),
            'format' => '?paged=%#%',
            'current' => max(1, get_query_var('paged')),
            'total' => $wp_query->max_num_pages
        ));
    }

    // Custom Excerpts
    function mpministry2015wp_index($length) // Create 20 Word Callback for Index page Excerpts, call using mpministry2015wp_excerpt('mpministry2015wp_index');
    {
        return 20;
    }

    // Create 40 Word Callback for Custom Post Excerpts, call using mpministry2015wp_excerpt('mpministry2015wp_custom_post');
    function mpministry2015wp_custom_post($length)
    {
        return 40;
    }

    // Create the Custom Excerpts callback
    function mpministry2015wp_excerpt($length_callback = '', $more_callback = '')
    {
        global $post;
        if (function_exists($length_callback)) {
            add_filter('excerpt_length', $length_callback);
        }
        if (function_exists($more_callback)) {
            add_filter('excerpt_more', $more_callback);
        }
        $output = get_the_excerpt();
        $output = apply_filters('wptexturize', $output);
        $output = apply_filters('convert_chars', $output);
        $output = '<p>' . $output . '</p>';
        echo $output;
    }

    // Custom View Article link to Post
    function mpministry2015_blank_view_article($more)
    {
        global $post;
        return '... <a class="view-article" href="' . get_permalink($post->ID) . '">' . __('View Article', 'mpministry2015') . '</a>';
    }

    // Remove 'text/css' from our enqueued stylesheet
    function mpministry2015_style_remove($tag)
    {
        return preg_replace('~\s+type=["\'][^"\']++["\']~', '', $tag);
    }

    // Remove thumbnail width and height dimensions that prevent fluid images in the_thumbnail
    function remove_thumbnail_dimensions( $html )
    {
        $html = preg_replace('/(width|height)=\"\d*\"\s/', "", $html);
        return $html;
    }

    // Custom Gravatar in Settings > Discussion
    function mpministry2015gravatar ($avatar_defaults)
    {
        $myavatar = get_template_directory_uri() . '/img/gravatar.jpg';
        $avatar_defaults[$myavatar] = "Custom Gravatar";
        return $avatar_defaults;
    }

    // Threaded Comments
    function enable_threaded_comments()
    {
        if (!is_admin()) {
            if (is_singular() AND comments_open() AND (get_option('thread_comments') == 1)) {
                wp_enqueue_script('comment-reply');
            }
        }
    }

    // Custom Comments Callback
    function mpministry2015comments($comment, $args, $depth)
    {
        $GLOBALS['comment'] = $comment;
        extract($args, EXTR_SKIP);

        if ( 'div' == $args['style'] ) {
            $tag = 'div';
            $add_below = 'comment';
        } else {
            $tag = 'li';
            $add_below = 'div-comment';
        }
    ?>
        <!-- heads up: starting < for the html tag (li or div) in the next line: -->
        <<?php echo $tag ?> <?php comment_class(empty( $args['has_children'] ) ? '' : 'parent') ?> id="comment-<?php comment_ID() ?>">
        <?php if ( 'div' != $args['style'] ) : ?>
        <div id="div-comment-<?php comment_ID() ?>" class="comment-body">
        <?php endif; ?>
        <div class="comment-author vcard">
        <?php if ($args['avatar_size'] != 0) echo get_avatar( $comment, $args['180'] ); ?>
        <?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
        </div>
    <?php if ($comment->comment_approved == '0') : ?>
        <em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.') ?></em>
        <br />
    <?php endif; ?>

        <div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>">
            <?php
                printf( __('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','' );
            ?>
        </div>

        <?php comment_text() ?>

        <div class="reply">
        <?php comment_reply_link(array_merge( $args, array('add_below' => $add_below, 'depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
        </div>
        <?php if ( 'div' != $args['style'] ) : ?>
        </div>
        <?php endif; ?>
    <?php }

    /*------------------------------------*\
        Actions + Filters + ShortCodes
    \*------------------------------------*/

    // Add Actions
    add_action('init', 'mpministry2015_header_scripts'); // Add Custom Scripts to wp_head
    add_action('wp_print_scripts', 'mpministry2015_conditional_scripts'); // Add Conditional Page Scripts
    add_action('get_header', 'enable_threaded_comments'); // Enable Threaded Comments
    add_action('wp_enqueue_scripts', 'mpministry2015_styles'); // Add Theme Stylesheet
    add_action('init', 'register_mpministry2015_menu'); // Add mpministry2015 Blank Menu

    add_action('widgets_init', 'my_remove_recent_comments_style'); // Remove inline Recent Comment Styles from wp_head()
    add_action('init', 'mpministry2015wp_pagination'); // Add our mpministry2015 Pagination

    // Remove Actions
    remove_action('wp_head', 'feed_links_extra', 3); // Display the links to the extra feeds such as category feeds
    remove_action('wp_head', 'feed_links', 2); // Display the links to the general feeds: Post and Comment Feed
    remove_action('wp_head', 'rsd_link'); // Display the link to the Really Simple Discovery service endpoint, EditURI link
    remove_action('wp_head', 'wlwmanifest_link'); // Display the link to the Windows Live Writer manifest file.
    remove_action('wp_head', 'index_rel_link'); // Index link
    remove_action('wp_head', 'parent_post_rel_link', 10, 0); // Prev link
    remove_action('wp_head', 'start_post_rel_link', 10, 0); // Start link
    remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // Display relational links for the posts adjacent to the current post.
    remove_action('wp_head', 'wp_generator'); // Display the XHTML generator that is generated on the wp_head hook, WP version
    remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
    remove_action('wp_head', 'rel_canonical');
    remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

    // Add Filters
    add_filter('avatar_defaults', 'mpministry2015gravatar'); // Custom Gravatar in Settings > Discussion
    add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
    add_filter('widget_text', 'do_shortcode'); // Allow shortcodes in Dynamic Sidebar
    add_filter('widget_text', 'shortcode_unautop'); // Remove <p> tags in Dynamic Sidebars (better!)

    add_filter('the_category', 'remove_category_rel_from_category_list'); // Remove invalid rel attribute
    add_filter('the_excerpt', 'shortcode_unautop'); // Remove auto <p> tags in Excerpt (Manual Excerpts only)
    add_filter('the_excerpt', 'do_shortcode'); // Allows Shortcodes to be executed in Excerpt (Manual Excerpts only)
    add_filter('excerpt_more', 'mpministry2015_blank_view_article'); // Add 'View Article' button instead of [...] for Excerpts
    add_filter('style_loader_tag', 'mpministry2015_style_remove'); // Remove 'text/css' from enqueued stylesheet
    add_filter('post_thumbnail_html', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to thumbnails
    add_filter('image_send_to_editor', 'remove_thumbnail_dimensions', 10); // Remove width and height dynamic attributes to post images

    // Remove Filters
    remove_filter('the_excerpt', 'wpautop'); // Remove <p> tags from Excerpt altogether

    // Shortcodes
    add_shortcode('mpministry2015_shortcode_demo', 'mpministry2015_shortcode_demo'); // You can place [mpministry2015_shortcode_demo] in Pages, Posts now.
    add_shortcode('mpministry2015_shortcode_demo_2', 'mpministry2015_shortcode_demo_2'); // Place [mpministry2015_shortcode_demo_2] in Pages, Posts now.

    // Shortcodes above would be nested like this -
    // [mpministry2015_shortcode_demo] [mpministry2015_shortcode_demo_2] Here's the page title! [/mpministry2015_shortcode_demo_2] [/mpministry2015_shortcode_demo]

    /*------------------------------------*\
        Custom Post Types
    \*------------------------------------*/



    /*------------------------------------*\
        ShortCode Functions
    \*------------------------------------*/

    // Shortcode Demo with Nested Capability
    function mpministry2015_shortcode_demo($atts, $content = null)
    {
        return '<div class="shortcode-demo">' . do_shortcode($content) . '</div>'; // do_shortcode allows for nested Shortcodes
    }

    // Shortcode Demo with simple <h2> tag
    function mpministry2015_shortcode_demo_2($atts, $content = null) // Demo Heading H2 shortcode, allows for nesting within above element. Fully expandable.
    {
        return '<h2>' . $content . '</h2>';
    }



/****** Matt J's Functions ******/

    // Editor Style
    add_editor_style('style.css');

    // Add Menu Support
    add_theme_support('menus');

    // Add Thumbnail Theme Support
    add_theme_support('post-thumbnails');

    // REPLACE "current_page_" WITH CLASS "active"
    function current_to_active($text){
        $replace = array(
            // List of classes to replace with "active"
            'current_page_item' => 'active',
            'current_page_parent' => 'active',
            'current_page_ancestor' => 'active',
        );
        $text = str_replace(array_keys($replace), $replace, $text);
        return $text;
    }
    add_filter ('wp_nav_menu','current_to_active');

    //Page Slug Body Class
    function add_slug_body_class( $classes ) {
        global $post;
        if ( isset( $post ) ) {
            $classes[] = $post->post_type . '-' . $post->post_name;
        }
        return $classes;
    }
    add_filter( 'body_class', 'add_slug_body_class' );

    // Remove default links when adding images
    // function wpb_imagelink_setup() {
    function remove_default_image_link() {
        $image_set = get_option( 'image_default_link_type' );

        if ($image_set !== 'none') {
            update_option('image_default_link_type', 'none');
        }
    }
    add_action('admin_init', 'remove_default_image_link', 10);

    // Add page/post slug class to menu item classes
    function add_slug_class_to_menu_item($output){
        $iterator_add_slug_class_to_menu_item = 0;

        $ps = get_option('permalink_structure');
        if(!empty($ps)){
            $idstr = preg_match_all('/<li id="menu-item-(\d+)/', $output, $matches);
            foreach($matches[1] as $mid){
                $iterator_add_slug_class_to_menu_item++;

                $id = get_post_meta($mid, '_menu_item_object_id', true);

                // If first menu item slug is "home" (for "Home" page) and
                // if not then slug assigned to another sting
                if($iterator_add_slug_class_to_menu_item == 1) {
                    $slug = 'home';
                }
                else {
                    $slug = basename(get_permalink($id));
                }
                $output = preg_replace('/menu-item-'.$mid.'">/', 'menu-item-'.$mid.' menu-item-'.$slug.'">', $output, 1);
            }
        }
        return $output;
    }
    add_filter('wp_nav_menu', 'add_slug_class_to_menu_item');

    // Call the_post_thumbnail_caption(); to
    // display the caption of the thumbnail
    function the_post_thumbnail_caption() {
        global $post;

        $thumbnail_id    = get_post_thumbnail_id($post->ID);
        $thumbnail_image = get_posts(array('p' => $thumbnail_id, 'post_type' => 'attachment'));

        if ($thumbnail_image && isset($thumbnail_image[0])) {
            echo '<p class="thumbnail-caption">'.$thumbnail_image[0]->post_excerpt.'</p>';
        }
    }

    // In the TinyMCE Editor box
    // show links to pages ONLY and NOT posts (including custom post types)
    function removeCustomPostTypesFromTinyMCELinkBuilder($query){
      $key = false;

      $cpt_to_remove = array(
            'aboutpage',
            'contactpage',
            'homepage',
            'packagespage'
      );

      foreach ($cpt_to_remove as $custom_post_type) {
        $key = array_search($custom_post_type, $query['post_type']);
        if($key){
          unset($query['post_type'][$key]);
        }
      }
      return $query;
    }
    add_filter( 'wp_link_query_args', 'removeCustomPostTypesFromTinyMCELinkBuilder' );

    // Add dashicons to the front end
    function load_dashicons_front_end() {
        wp_enqueue_style( 'dashicons' );
    }
    add_action( 'wp_enqueue_scripts', 'load_dashicons_front_end' );