<?php
/*
Template Name: Packages Page
*/

/**
* Displays Packages page specific custom post types.
*/
get_header(); ?>

	<main role="main">

		<section id="content">

      <div class="wrapper">

        <h1><?php the_title(); ?></h1>

        <?php
        $packages_page_args = array(
              'post_type'         => 'packagespage',
              'orderby'           => 'title',
              'posts_per_page'    => 1
        );

        $packages_page_lib_query = new WP_Query($packages_page_args);
        if($packages_page_lib_query->have_posts()) {
          while ($packages_page_lib_query->have_posts()) : $packages_page_lib_query->the_post();
        ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php the_content(); ?>

          </article>

        <?php
        endwhile;
        wp_reset_postdata();
        }
        else {
        ?>

          <article>

            <h2>Sorry, nothing to display.</h2>

          </article>

        <?php
        }
        ?>

      </div><?php // .wrapper END ?>

		</section>
	</main>

<?php get_footer(); ?>