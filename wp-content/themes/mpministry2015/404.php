<?php get_header(); ?>

<main role="main">

	<section id="content">

		<div class="wrapper">
			<h1>oops!</h1>
			<article id="post-404">

				<h2>Don't worry, it's not serious, but it looks like the page you've requested isn't available. It may have been removed, had its name changed, or is temporarily unavailable.</h2>

				<p>Please try one of the following:</p>

        <ul>
          <li>If you typed the address in the address bar, make sure that it is spelled correctly.</li>
          <li>Use the menu at the top and look for links to the information you want.</li>
          <li>Follow <a href="<?php echo esc_url( home_url( '/' ) ); ?>">this link to our Home page</a> and look for links to the information you want.</li>
        </ul>

				<br class="clear">

			</article>

		</div><?php // .wrapper END ?>

	</section>
</main>

<?php get_footer(); ?>
