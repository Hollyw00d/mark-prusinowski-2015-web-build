<!doctype html>
<html <?php language_attributes(); ?> class="no-js">

<head>
	<meta charset="<?php bloginfo('charset'); ?>">

	<?php
	// Ternary if, elseif, else statement below for TITLE tag that says
	// if on home page at start of tag display 'Home',
  // elseif get_the_title() doesn't exist (404 error) display 'Page Not Found'
  // else display get_the_title()
  ?>
  <title><?php echo (get_the_title() == 'Home') ? 'Home' : ( (!get_the_title()) ? 'Page Not Found' : get_the_title() ); ?> | <?php bloginfo('name'); ?> - <?php bloginfo('description'); ?></title>

	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/favicon.ico" rel="shortcut icon">
	<link href="<?php echo get_template_directory_uri(); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">

	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="<?php bloginfo('description'); ?>">

	<?php wp_head(); ?>
	<script>
	// conditionizr.com
	// configure environment tests
	conditionizr.config({
		assets: '<?php echo get_template_directory_uri(); ?>',
		tests: {}
	});
	</script>

</head>

<body <?php body_class(); ?>>

	<header class="header clear" role="banner">

		<div class="wrapper">

			<a id="logo" href="<?php echo home_url(); ?>"><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?><?php bloginfo('name'); ?></a>

			<div id="contact-info">
				<a href="mailto:mark@mpministry.com">mark@mpministry.com</a> &bull; 253-304-8445
			</div>

			<?php wp_nav_menu(array('menu' => 'Desktop Menu', 'container' => 'nav', 'container_id' => 'desktop-nav')); ?>

			<span id="mobile-nav-button" class="dashicons dashicons-menu">Main Menu</span>

    </div><?php // .wrapper END ?>

    <?php wp_nav_menu(array('menu' => 'Mobile Menu', 'container' => 'nav', 'container_id' => 'mobile-nav')); ?>

	</header>