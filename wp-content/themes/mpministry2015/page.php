<?php get_header(); ?>

	<main role="main">

		<section id="content">

			<div class="wrapper">
        <h1><?php the_title(); ?></h1>

        <?php if (have_posts()): while (have_posts()) : the_post(); ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <?php the_content(); ?>

            <br class="clear">

          </article>

        <?php endwhile; ?>

        <?php else: ?>

          <article>

            <h2>Sorry, nothing to display.</h2>

            <br class="clear">

          </article>

        <?php endif; ?>

			</div><?php // .wrapper END ?>

		</section>
	</main>

<?php get_footer(); ?>