<?php
/*
Template Name: Contact Page
*/

/**
* Displays Contact page specific custom post types.
*/
get_header(); ?>

	<main role="main">

		<section id="content">

      <div class="wrapper">

        <h1><?php the_title(); ?></h1>

        <?php
        $contact_page_args = array(
              'post_type'         => 'contactpage',
              'orderby'           => 'title',
              'posts_per_page'    => 1
        );

        $contact_page_lib_query = new WP_Query($contact_page_args);
        if($contact_page_lib_query->have_posts()) {
          while ($contact_page_lib_query->have_posts()) : $contact_page_lib_query->the_post();
        ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="row">
              <div class="col-md-7">
                <?php the_content(); ?>
                <h3><a href="mailto:<?php echo get_post_meta($post->ID, '_contact_page_email', true); ?>"><?php echo get_post_meta($post->ID, '_contact_page_email', true); ?></a></h3>

                <?php
                // Phone number NO dashes
                $phone_no_dashes = get_post_meta($post->ID, '_contact_page_phone_number', true);

                // Phone number WITH dashes
                $phone_with_dashes = substr($phone_no_dashes, 0, 3);
                $phone_with_dashes .= '-' . substr($phone_no_dashes, 3, 3);
                $phone_with_dashes .= '-' . substr($phone_no_dashes, 6, 4);
                ?>

                <?php
                // Show phone number without link on desktop and
                // WITH link on mobile screen sizes
                ?>
                <h3 class="mobile-hide black-text"><?php echo $phone_with_dashes; ?></h3>
                <h3 class="mobile-show"><a href="tel:+1<?php echo $phone_no_dashes; ?>"><?php echo $phone_with_dashes; ?></a></h3>
              </div>

              <div class="col-md-5">
                <div id="featured-image-container">
                  <?php
                  the_post_thumbnail();
                  ?>
                </div>
              </div>
            </div>

          </article>

        <?php
        endwhile;
        wp_reset_postdata();
        }
        else {
        ?>

          <article>

            <h2>Sorry, nothing to display.</h2>

          </article>

        <?php
        }
        ?>

      </div><?php // .wrapper END ?>

		</section>
	</main>

<?php get_footer(); ?>