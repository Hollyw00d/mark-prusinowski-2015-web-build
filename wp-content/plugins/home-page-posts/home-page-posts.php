<?php
/**
 * Plugin Name: Home Page Posts
 * Plugin URI: http://www.mattjennings.net/
 * Description: Plugin displays Home page custom post types.
 * Author: Matt Jennings
 * Author URI: http://www.mattjennings.net/
 * Version: 0.0.1
 * License: GPLv2
 */
// Exit if accessed directly
if(!defined('ABSPATH')) {
  exit;
}


/******
 * Home Page Post Type START
 ******/

function home_page_post_type() {
  $singular = 'Home Page';
  $plural = $singular;
  $labels = array(
        'name'                  => $plural,
        'singular_name'         => $singular,
        'add_name'              => 'Add New',
        'add_new_item'          => 'Add New ' . $singular,
        'edit'                  => 'Edit',
        'edit_item'             => 'Edit ' . $singular,
        'new_item'              => 'New ' . $singular,
        'view'                  => 'View ' . $singular,
        'view_item'             => 'View' . $singular,
        'search_term'           => 'Search ' . $plural,
        'parent'                => 'Parent ' . $singular,
        'not_found'             => 'No ' . $plural . ' found',
        'not_found_in_trash'    => 'No ' . $plural . ' in Trash'
  );
  $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'show_in_nav_menus'     => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_admin_bar'     => true,
        'menu_position'         => 50,
        'menu_icon'             => 'dashicons-lightbulb',
        'can_export'            => true,
        'delete_with_user'      => false,
        'hierarchical'          => false,
        'has_archive'           => false,
        'query_var'             => true,
        'capability_type'       => 'post',
        'map_meta_cap'          => true,
        'rewrite'               => array(
              'slug'                  => 'home-page-posts',
              'with_front'            => true,
              'pages'                 => true,
              'feeds'                 => true
        ),
        'supports'              => array(
              'title'
        )
  );
  register_post_type('homepage', $args);
}
add_action('init', 'home_page_post_type');

function home_page_add_meta_box() {
  add_meta_box(
        'home_page_id',
        __( 'Home Page Text', 'home_page_plugin' ),
        'home_page_meta_box_callback',
        'homepage'
  );
}

function home_page_meta_box_callback($post) {
  wp_nonce_field( 'home_page_save_meta_box_data', 'home_page_meta_box_nonce' );

  $home_page_heading_value = get_post_meta($post->ID, '_home_page_heading', true);
  $home_page_sub_heading_value = get_post_meta($post->ID, '_home_page_sub_heading', true);
  $home_page_btn_text_value = get_post_meta($post->ID, '_home_page_btn_text', true);
  $home_page_btn_link_value = get_post_meta($post->ID, '_home_page_btn_link', true);

  echo '<p><label for="home_page_heading">Big Heading</label><br /><input type="text" name="home_page_heading" id="' . $post->ID . '" value="' . $home_page_heading_value . '" style="width: 100%;" /></p>';
  echo '<p><label for="home_page_sub_heading">Sub Heading</label><br /><textarea type="text" name="home_page_sub_heading" id="' . $post->ID . '" style="width: 100%;" rows="3">' . $home_page_sub_heading_value . '</textarea></p>';

  // Spacer paragraph
  echo '<p>&nbsp;</p>';

  echo '<p><label for="home_page_btn_text">Button Text</label><br /><input type="text" name="home_page_btn_text" id="' . $post->ID . '" value="' . $home_page_btn_text_value . '" style="width: 100%;" /></p>';
  echo '<p><label for="home_page_btn_link">Button Link</label><br /><input type="text" name="home_page_btn_link" id="' . $post->ID . '" value="' . $home_page_btn_link_value . '" style="width: 100%;" /></p>';
}

add_action( 'add_meta_boxes', 'home_page_add_meta_box' );

function home_page_save_meta_box_data( $post_id ) {

  if ( !isset($_POST['home_page_meta_box_nonce']) ) {
    return;
  }

  if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
    return;
  }

  if ( ! current_user_can( 'edit_post', $post_id ) ) {
    return;
  }

  if ( !isset($_POST['home_page_heading']) && !isset($_POST['home_page_sub_heading']) && !isset($_POST['home_page_btn_text']) && !isset($_POST['home_page_btn_link'])) {
    return;
  }

  $data_home_page_heading = sanitize_text_field( $_POST['home_page_heading'] );
  $data_home_page_sub_heading = sanitize_text_field( $_POST['home_page_sub_heading'] );
  $data_home_page_btn_text = sanitize_text_field( $_POST['home_page_btn_text'] );
  $data_home_page_btn_link = sanitize_text_field( $_POST['home_page_btn_link'] );

  update_post_meta( $post_id, '_home_page_heading', $data_home_page_heading );
  update_post_meta( $post_id, '_home_page_sub_heading', $data_home_page_sub_heading );
  update_post_meta( $post_id, '_home_page_btn_text', $data_home_page_btn_text );
  update_post_meta( $post_id, '_home_page_btn_link', $data_home_page_btn_link );
}

add_action( 'save_post', 'home_page_save_meta_box_data' );

/******
 * Home Page Post Type END
 ******/