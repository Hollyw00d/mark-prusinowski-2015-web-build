<footer class="footer" role="contentinfo">
  <p>
    &copy; Mark Prusinowski <?php echo date('Y'); ?>. All Rights Reserved.
  </p>
</footer>

<?php wp_footer(); ?>

</body>
</html>