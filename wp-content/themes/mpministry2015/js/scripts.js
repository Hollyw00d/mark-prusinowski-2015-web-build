// Start anonymous function
(function() {

  // jQuery Document Ready
  jQuery(document).ready(function($) {

    // Mobile nav hamburger menu click event to
    // show the mobile nav drop down
    $("#mobile-nav-button").on("click", function() {
      $("#mobile-nav").slideToggle();
    });

    // Window reset event to hide the mobile nav
    $(window).on("resize", function() {
        $("#mobile-nav").removeAttr("style");
    });

  });

})();