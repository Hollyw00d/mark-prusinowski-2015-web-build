<?php
define('WP_CACHE', true); //Added for Hyper Cache plugin
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// Localhost found @:
// localhost:8888
if ( $_SERVER['SERVER_NAME'] == "localhost" || preg_match('/^192\.168/', $_SERVER['SERVER_NAME']) > 0 )
{
	define('DB_NAME', 'mpministry2015db');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_HOST', 'localhost');
}
// Staging found @:
// mp2015.mattjennings.net
else if ( $_SERVER['SERVER_NAME'] == "www.mp2015.mattjennings.net" )
{
	define('DB_NAME', 'mpministry2015db');
	define('DB_USER', 'mattpjennings');
	define('DB_PASSWORD', '666dmc7106');
	define('DB_HOST', 'mysql.mp2015.mattjennings.net');
}
else // production environment
{
	define('DB_NAME', 'productionInfo');
	define('DB_USER', 'productionInfo');
	define('DB_PASSWORD', 'productionInfo');
	define('DB_HOST', 'productionInfo');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|jrLQbO<sry,S|(MzcK.,YwVPcZLqUE0[QkO1GsXQLi2g+!D]N30{,QG5cwi<H#C');
define('SECURE_AUTH_KEY',  'Myf8U8vy;QyN+9v4A;G_Owydu* p[<ii-}[8-`B~oN^94V<J|{H:>P+eG`e)Z!1*');
define('LOGGED_IN_KEY',    '2J1pb-RQoxd1}w[ #&bQ|PehqJw8<./?UW<+Qh|R6|Nl-_g%)JRM:S091%($4AU]');
define('NONCE_KEY',        '?5E-!T.xy{AQ&ibA}u,3rp[);@OI@}8ohE@Rz{9b[H%nn]G$N,<;];|-1K*=XM~~');
define('AUTH_SALT',        'Mn3b[aG=*9DiqX.%Ke7P94o7jyMP2Z|;Jml%C0`z25KiO]@1~<QU omC,ku=l40r');
define('SECURE_AUTH_SALT', '-=|O&5W.O$+&qQwVny)fsenlK5gY.4e<p;&}lzVR3q.H`%Ep]L)Xc_|#-f~/L#y)');
define('LOGGED_IN_SALT',   '<i| {+.l.B@p3>nc6nKI(<f[#X64n0kt|n9+ p[dkz-K,f|_ot$8-kC0[GC11$=-');
define('NONCE_SALT',       'm 8)2|r|;]Rm|r0ctW04^LStW+r_R^y}gjN|u6`<S|7HWfXTrp(@$}3}N6P 5Y,~');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'briezoup_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
