<?php
/**
 * Plugin Name: About Page Posts
 * Plugin URI: http://www.mattjennings.net/
 * Description: Plugin displays About page custom post types.
 * Author: Matt Jennings
 * Author URI: http://www.mattjennings.net/
 * Version: 0.0.1
 * License: GPLv2
 */
// Exit if accessed directly
if(!defined('ABSPATH')) {
  exit;
}


/******
 * About Page Post Type START
 ******/

function about_page_post_type() {
  $singular = 'About Page';
  $plural = $singular;
  $labels = array(
        'name'                  => $plural,
        'singular_name'         => $singular,
        'add_name'              => 'Add New',
        'add_new_item'          => 'Add New ' . $singular,
        'edit'                  => 'Edit',
        'edit_item'             => 'Edit ' . $singular,
        'new_item'              => 'New ' . $singular,
        'view'                  => 'View ' . $singular,
        'view_item'             => 'View' . $singular,
        'search_term'           => 'Search ' . $plural,
        'parent'                => 'Parent ' . $singular,
        'not_found'             => 'No ' . $plural . ' found',
        'not_found_in_trash'    => 'No ' . $plural . ' in Trash'
  );
  $args = array(
        'labels'                => $labels,
        'public'                => true,
        'publicly_queryable'    => true,
        'exclude_from_search'   => false,
        'show_in_nav_menus'     => false,
        'show_ui'               => true,
        'show_in_menu'          => true,
        'show_in_admin_bar'     => true,
        'menu_position'         => 50,
        'menu_icon'             => 'dashicons-businessman',
        'can_export'            => true,
        'delete_with_user'      => false,
        'hierarchical'          => false,
        'has_archive'           => false,
        'query_var'             => true,
        'capability_type'       => 'post',
        'map_meta_cap'          => true,
        'rewrite'               => array(
              'slug'                  => 'about-page-posts',
              'with_front'            => true,
              'pages'                 => true,
              'feeds'                 => true
        ),
        'supports'              => array(
              'title',
              'editor',
              'thumbnail'
        )
  );
  register_post_type('aboutpage', $args);
}
add_action('init', 'about_page_post_type');

/******
 * About Page Post Type END
 ******/