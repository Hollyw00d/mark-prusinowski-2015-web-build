<?php
/*
Template Name: Home Page
*/

/**
* Displays Home page specific custom post types.
*/
get_header(); ?>

	<main role="main">

		<section id="content" style="background-image: url(<?php bloginfo('template_directory'); ?>/img/home-page/home<?php echo rand(1,3); ?>.jpg);">

			<div class="wrapper">

        <?php
        $home_page_args = array(
              'post_type'         => 'homepage',
              'orderby'           => 'title',
              'posts_per_page'    => 1
        );

        $home_page_lib_query = new WP_Query($home_page_args);
        if($home_page_lib_query->have_posts()) {
          while ($home_page_lib_query->have_posts()) : $home_page_lib_query->the_post();
        ?>

            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

              <h1><?php echo get_post_meta($post->ID, '_home_page_heading', true); ?></h1>

              <h2><?php echo get_post_meta($post->ID, '_home_page_sub_heading', true); ?></h2>

              <p class="center"><a class="button-link" href="<?php echo get_post_meta($post->ID, '_home_page_btn_link', true); ?>"><?php echo get_post_meta($post->ID, '_home_page_btn_text', true); ?></a></p>

              <br class="clear">

            </article>

        <?php
          endwhile;
          wp_reset_postdata();
        }
        else {
        ?>

          <article>

            <h2>Sorry, nothing to display.</h2>

            <br class="clear">

          </article>

        <?php
        }
        ?>

			</div><?php // .wrapper END ?>

		</section>
	</main>

<?php get_footer(); ?>