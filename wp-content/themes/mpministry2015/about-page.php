<?php
/*
Template Name: About Page
*/

/**
* Displays About page specific custom post types.
*/
get_header(); ?>

	<main role="main">

		<section id="content">

      <div class="wrapper">

        <h1><?php the_title(); ?></h1>

        <?php
        $about_page_args = array(
              'post_type'         => 'aboutpage',
              'orderby'           => 'title',
              'posts_per_page'    => 1
        );

        $about_page_lib_query = new WP_Query($about_page_args);
        if($about_page_lib_query->have_posts()) {
          while ($about_page_lib_query->have_posts()) : $about_page_lib_query->the_post();
        ?>

          <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="row">
              <div class="col-md-7">
                <?php the_content(); ?>
              </div>

              <div class="col-md-5">
                <div id="featured-image-container">
                  <?php
                  the_post_thumbnail();
                  ?>
                </div>
              </div>
            </div>

          </article>

        <?php
        endwhile;
        wp_reset_postdata();
        }
        else {
        ?>

          <article>

            <h2>Sorry, nothing to display.</h2>

          </article>

        <?php
        }
        ?>

      </div><?php // .wrapper END ?>

		</section>
	</main>

<?php get_footer(); ?>